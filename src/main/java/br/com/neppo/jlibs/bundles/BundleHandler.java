package br.com.neppo.jlibs.bundles;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class BundleHandler {

    private BundleHandler(){}

    protected static Logger LOG = LoggerFactory.getLogger(BundleHandler.class);

    private static Map<String, Object> bundle = null;

    private static File file = null;
    private static InputStream fileStream = null;

    private static boolean isFilled(){
        if(file == null && fileStream == null) return false;
        if(file != null && file.isFile() && file.exists()) return true;
        try{
            if(fileStream.available() == 0){
                fileStream.reset();
            }
        }
        catch (Exception e){
            LOG.warn("Could not peer into the the file stream.", e);
            return false;
        }
        return true;
    }
    public static void setStream(InputStream stream){
        if(isFilled()){
            return;
        }
        fileStream = stream;
    }
    public static void setFile(File ref){
        if(isFilled()){
            return;
        }
        file = ref;
    }


    private static String correctString(String s){
        return StringUtils.isEmpty(s) ? "" : s;
    }

    private static void loadBundle(){
        if(bundle == null && isFilled()){

            try{
                InputStream stream;
                if(file != null){
                    stream = new FileInputStream(file);
                }
                else {
                    stream = fileStream;
                }
                String json = new BufferedReader(new InputStreamReader(stream))
                        .lines()
                        .parallel()
                        .collect(Collectors.joining(""))
                        .replaceAll("(^\\s+|\\s+$)", "");

                // converts bundle to map
               ObjectMapper mapper = new ObjectMapper();
               bundle = mapper.readValue(json, new TypeReference<Map<String, Object>>(){});
            }
            catch (IOException e){
                LOG.error(e.getMessage(), e);
            }

        }
    }

    private static Object search(Map<String, Object> map, String[] keys, int pos){
        if(pos >= keys.length){
            return null;
        }

        if(map == null || map.isEmpty()){
            return null;
        }

        Object value = map.get(keys[pos]);

        if(value == null){
            return null;
        }

        if(pos == keys.length - 1){
            return value;
        }

        if(value instanceof Map){
            return search((Map) value, keys, pos + 1);
        }

        return null;
    }

    public static Object searchBundle(String s){
    	loadBundle();
        return search(bundle, correctString(s).split("\\."), 0);
    }

    private static String stringOrNull(Object o){
        if(o instanceof String){
            return (String) o;
        }
        return null;
    }

    public static String resolveOkMessages(String code, String defaultmsg){
        return resolveMessage(200, code, null, defaultmsg);
    }

    public static String resolveMessage(Integer status, String code, String field, String defaultMessage){
        return resolveMessage(status, code, field, defaultMessage, null);
    }

    private static boolean isNotEmptyPlaceHolder(String[] placeholdersOfMessage){
        return placeholdersOfMessage != null && placeholdersOfMessage.length > 0;
    }

    public static String resolveMessage(Integer status, String code, String field, String defaultMessage, String... placeholdersOfMessage){
        String finalMessage = "";
        if (status != null && isNotBlank(code)) {
            loadBundle();

            // treats lists
            if ((isNotBlank(field) || isNotEmptyPlaceHolder(placeholdersOfMessage)) && status >= 400) {
                String finalMessageAux = stringOrNull(searchBundle("Modifiable.Errors." + code));

                if (isNotBlank(finalMessageAux)) {
                    if (isNotEmptyPlaceHolder(placeholdersOfMessage)) {
                        finalMessage = String.format(finalMessageAux, placeholdersOfMessage);
                    } else if (isNotBlank(field)) {
                        finalMessage = String.format(finalMessageAux, treatLists(field));
                    }


                    if (finalMessage != null && finalMessage.equals(finalMessageAux)) {
                        finalMessage = null;
                    }

                }
            }

            // treats static messages
            if (isBlank(finalMessage)) {
                if (status >= 400) {
                    finalMessage = stringOrNull(searchBundle("Concrete.Errors." + code));
                } else {
                    finalMessage = stringOrNull(searchBundle("Concrete.Messages." + code));
                }
            }

            if (isBlank(finalMessage)) {
                finalMessage = defaultMessage;
            }
        }

        return finalMessage;
    }

    private static Pattern listPattern = Pattern.compile("^[\\w\\-\\.]+(,[\\w\\-\\.]+)+$");

    private static String treatLists(String listed){

        String separator = (String) ObjectUtils.defaultIfNull(searchBundle("Listing.Grammar.separator"), ",");
        String and = (String) ObjectUtils.defaultIfNull(searchBundle("Listing.Grammar.and"), ",");
        String oxford = (String) ObjectUtils.defaultIfNull(searchBundle("Listing.Grammar.oxford"), "");


        // ignore empty or null strings
        if(Strings.isBlank(listed)){
            return listed;
        }

        Matcher matcher = listPattern.matcher(listed);

        // treats one or less items
        if(!matcher.matches()){
            return listed;
        }

        String[] list = listed.split(",");

        StringBuilder builder = new StringBuilder();

        //treat 3+ items
        for (int i = 0; i < list.length - 1 ; i++){

            builder.append(list[i]).append(list.length == 2 ? " " : "");

            if(i != list.length - 2){
                builder.append(separator).append(' ');
            }
            else{
                if(list.length > 2){
                    builder.append(oxford).append(' ');
                }
                builder.append(and).append(' ');
            }

        }

        //last item
        builder.append(list[list.length - 1]);

        return builder.toString();
    }
}